const api_gateway_url = 'https://eokj67zeaa.execute-api.ap-southeast-2.amazonaws.com/prod';
const messages = ['Segmenting image ...','Creating Skeleton ...','Finding thumb baseline ...','Defining baseline vector ...','Finding tip of thumb ...', 'Defining thumb vector', 'Calculating angle...'];
const the_event = 'general';

function openModal() {
  document.getElementById('modal').style.display = 'block';
}

function closeModal() {
  document.getElementById('modal').style.display = 'none';
}

function ordinal_suffix_of(i) {
  var j = i % 10,
      k = i % 100;
  if (j == 1 && k != 11) {
      return i + "st";
  }
  if (j == 2 && k != 12) {
      return i + "nd";
  }
  if (j == 3 && k != 13) {
      return i + "rd";
  }
  return i + "th";
}

function addCanvasToPage(canvas, name, mime) {
  var imageContainer = document.getElementById('filesInfo');
  imageContainer.setAttribute("data-name", name);
  imageContainer.setAttribute("data-mime", mime);
  imageContainer.innerHTML = "";
  imageContainer.appendChild(canvas);
}

function processFiles() {
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    const file = this.files[0];
    if (!file) {
      return;
    } else if (file.size == 0) {
      alert('The selected file is empty.');
      return;
    }
    if (file.type.match('image.*')) {
      document.getElementById('summary').style.visibility = "hidden";
      openModal();
      processPhoto(file);
    } else {
      alert('The selected file does not appear to be an image.');
    }
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }
}

function processPhoto(file) {
  var name = file.name;
  var mime = file.type;
  var reader = new FileReader();
  reader.onload = function (event) {
    var image = new Image();
    image.onload = function () {
      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext("2d");
      var maxSize = 600;
      var tempW = image.width;
      var tempH = image.height;
      var scale = 1;

      if (image.width > image.height) {
        if (image.width > maxSize) {
          tempH = image.height * maxSize / image.width;
          tempW = maxSize;
          scale = maxSize / image.width;
        }
      } else {
        if (image.height > maxSize) {
          tempH = maxSize;
          tempW = image.width * maxSize / image.height;
          scale = maxSize / image.height;
        }
      }

      EXIF.getData(this, function () {
        ctx.save();

        var ori = EXIF.getTag(this, "Orientation");

        if (4 < ori && ori < 9) {
          canvas.width = tempH;
          canvas.height = tempW;
        } else {
          canvas.width = tempW;
          canvas.height = tempH;
        }

        ctx.scale(scale, scale);

        switch (ori) {
          case 4:
            ctx.scale(-1, 1);
            ctx.translate(-image.width, 0);
          case 3:
            ctx.translate(image.width / 2, image.height / 2);
            ctx.rotate(180 * Math.PI / 180);
            break;
          case 5:
            ctx.scale(1, -1);
            ctx.translate(0, -image.width);
          case 6:
            ctx.translate(image.height / 2, image.width / 2);
            ctx.rotate(90 * Math.PI / 180);
            break;
          case 7:
            ctx.scale(1, -1);
            ctx.translate(0, -image.width);
          case 8:
            ctx.translate(image.height / 2, image.width / 2);
            ctx.rotate(270 * Math.PI / 180);
            break;
          case 2:
            ctx.scale(-1, 1);
            ctx.translate(-image.width, 0);
          default:
            ctx.translate(image.width / 2, image.height / 2);
            break;
        }

        ctx.drawImage(this, -image.width / 2, -image.height / 2);
        ctx.restore();
        addCanvasToPage(canvas, name, mime);
        sendData();
      });

    }
    image.src = reader.result;
  }
  reader.readAsDataURL(file);
}

function sendData() {
  var photo = document.getElementById("filesInfo");
  var mime = photo.getAttribute("data-mime");
  var canvas = photo.children[0];
  var dataURL = canvas.toDataURL(mime);

  var xhr = new XMLHttpRequest();

  xhr.onload = function (e) {
    console.log(xhr.response);
    var obj = JSON.parse(xhr.response);
    if (xhr.status == 200) {
      replace_text(obj.angle, obj.percentile);
      replace_image(obj.image);
    } else {
      console.log(obj);
      alert(obj.message);
    }
    closeModal();
  }

  xhr.onerror = function (error) {
    console.log(error);
    alert("Something went wrong.");
    closeModal();
  }

  xhr.open("POST", api_gateway_url + "/thumbs?event=" + the_event, true);
  xhr.send(dataURL);
}

// get the image back
function replace_image(image_url){
  var myImage = new Image();

  myImage.onload = function () {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext("2d");
    ctx.save();
    addCanvasToPage(myImage, "new", myImage.type);
  }

  myImage.onerror = function (error) {
    console.log(error);
    alert("Something went wrong.");
  }

  myImage.src = image_url;
};

function replace_text(angle, percent) {
  document.getElementById('angle').innerHTML = angle.toFixed(2);
  document.getElementById('percent').innerHTML = ordinal_suffix_of(percent.toFixed(0));
  document.getElementById('summary').style.visibility = "visible";
}

document.getElementById("file").addEventListener("change", processFiles);
