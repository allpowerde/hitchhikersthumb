#!/bin/bash
FUNCTION_NAME="webpage"

DIR=$(dirname $0)

echo "*** Minify JS and CSS"
# minifying a CSS file (e.g. ready.js -> ready.min.js)
rm -f ${DIR}/css/*.min.css ${DIR}/js/*.min.js
curl -X POST -s --data-urlencode 'input@css/skeleton.css' https://cssminifier.com/raw > css/skeleton.min.css
curl -X POST -s --data-urlencode 'input@css/custom.css' https://cssminifier.com/raw > css/custom.min.css
curl -X POST -s --data-urlencode 'input@css/fonts.css' https://cssminifier.com/raw > css/fonts.min.css
# curl -X POST -s --data-urlencode 'input@css/normalize.css' https://cssminifier.com/raw > css/normalize.min.css

# cat ${DIR}/css/normalize.min.css ${DIR}/css/skeleton.min.css ${DIR}/css/custom.min.css ${DIR}/css/fonts.min.css > ${DIR}/css/cryptobreeder.css
cat ${DIR}/css/skeleton.min.css ${DIR}/css/custom.min.css ${DIR}/css/fonts.min.css > ${DIR}/css/hitchhikers.css

# test only
# cat ${DIR}/css/normalize.css ${DIR}/css/skeleton.css ${DIR}/css/custom.css ${DIR}/css/fonts.css > ${DIR}/css/hitchhikers.css

# minifying a JS file (e.g. ready.js -> ready.min.js)
curl -X POST -s --data-urlencode 'input@js/site.js' https://javascript-minifier.com/raw > js/site.min.js
curl -X POST -s --data-urlencode 'input@js/run_prettify.js' https://javascript-minifier.com/raw > js/run_prettify.min.js

cat ${DIR}/js/site.min.js > ${DIR}/js/hitchhikers.js

# test only
# cat ${DIR}/js/site.js > ${DIR}/js/hitchhikers.js

rm -f ${DIR}/css/*.min.css ${DIR}/js/*.min.js

#zip -r -u webpage.zip LICENSE.md NOTICE.md index.html privacy.html robots.txt status_latest.txt google1a6245f17cbc422d.html sitemap.xml js/ css/ img/

#aws s3 sync s3://wildrydes-us-east-1/WebApplication/1_StaticWebHosting/website s3://YOUR_BUCKET_NAME --region YOUR_BUCKET_REGION
aws s3 sync . s3://hitchhikersthumbweb/webpage/
