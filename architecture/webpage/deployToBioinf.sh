#TASK="Syncfull" # Sync full otherwise comment out to only sync js to production
#DRY="--dry-run"

REPO="general"
#PROD="c3dis"
#PROD="AWSinnoMumbai"
PROD="demoMay"
TEST="test"


if [ "$TASK" = "Syncfull" ]; then
    echo "--Full sync--"
    # Production
    sed -i .bak 's/the_event = '\'''$REPO''\''/the_event = '\'''$PROD''\''/g' ./js/thumb-select.js
    echo "--production replaced line:" $(sed -n 3p ./js/thumb-select.js)
    echo "--synced line:"
    rsync --group=thumb -rv ./* $(whoami)@bioinformatics.csiro.au:/var/www/html/thumb/ $DRY

    # Test
    sed -i .bak 's/the_event = '\'''$PROD''\''/the_event = '\'''$TEST''\''/g' ./js/thumb-select.js
    echo "--test replaced line:" $(sed -n 3p ./js/thumb-select.js)
    echo "--synced line:"
    rsync --group=thumb -rv ./* $(whoami)@bioinformatics.csiro.au:/var/www/html/thumbtest/ $DRY

    #back to "general"
    sed -i .bak 's/the_event = '\'''$TEST''\''/the_event = '\'''$REPO''\''/g' ./js/thumb-select.js
    echo "--repository replaced line:" $(sed -n 3p ./js/thumb-select.js)

else
    echo "--sync only thumbs file to production--"
    sed -i .bak 's/the_event = '\'''$REPO''\''/the_event = '\'''$PROD''\''/g' ./js/thumb-select.js
    echo "--replaced line:" $(sed -n 3p ./js/thumb-select.js)
    echo "--synced line:"
    rsync --group=thumb -rv ./js/thumb-select.js $(whoami)@bioinformatics.csiro.au:/var/www/html/thumb/js/ $DRY

    #back to "general"
    sed -i .bak 's/the_event = '\'''$PROD''\''/the_event = '\'''$REPO''\''/g' ./js/thumb-select.js
    echo "--repository replaced line:" $(sed -n 3p ./js/thumb-select.js)
fi;
