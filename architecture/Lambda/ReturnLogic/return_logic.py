import boto3
import json
import matplotlib.pyplot as pl
import os

from botocore.exceptions import ClientError
from base64 import b64encode
from boto3.dynamodb.conditions import Key, Attr
from io import BytesIO

thumbs_bucket = os.getenv('THUMBS_BUCKET', 'hitchhikersthumbprod')
thumbs_table = os.getenv('THUMBS_TABLE', 'HitchhikersThumb')
headers = {'Access-Control-Allow-Origin': '*'}
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(thumbs_table)
s3 = boto3.client('s3')

image_cache = {}

# read out DynoamoDB to get all angles, by 'event' if specified. Unordered.
def getAllAngles(event):
    if event is None or event == 'null' or event == '':
        response = table.scan(
            IndexName='EventAngleIndex'
        )
    else:
        response = table.scan(
            IndexName='EventAngleIndex',
            FilterExpression=Attr('event').eq(event)
        )
    results = response[u'Items']
    return results


def getFromS3(id):
    object_name = 'UploadedImages/img{}.jpg'.format(id)
    try:
        s3_response_object = s3.get_object(Bucket=thumbs_bucket, Key=object_name)
        image = s3_response_object['Body'].read()
        return image
    except ClientError as e:
        print('Unexpected error for file {}/{}: {}'.format(thumbs_bucket, object_name, e))
    

# Convert  image into utf-8 encoded base64
def binarizeImage(image):
    return b64encode(image).decode('utf-8')


def getTopXThatExist(number, event):
    results = getAllAngles(event)
    images=[]
    for r in reversed(results):
        res = getFromS3(r['id'])
        if res:
            images.append(binarizeImage(res))
            if len(images) == number:
                return images[number - 1]


def getHistogram(event):
    results = getAllAngles(event)

    cache_key = '{}{}{}'.format(len(results), event, 'histogram')
    if cache_key in image_cache:
        return image_cache[cache_key]

    event = 'all' if event is None or event == 'null' or event == '' else event
    pl.figure()
    pl.hist([int(i['angle']) for i in results], density=0)
    pl.title('Observed Thumb Angles ({})'.format(event))
    pl.xlabel('Value')
    pl.ylabel('Frequency')
    f = BytesIO()
    pl.savefig(f, format='jpg')
    image_cache[cache_key] = binarizeImage(f.getvalue())
    return image_cache[cache_key]


def getByID(id):
    image = getFromS3(id)
    if (image):
        return binarizeImage(image)
    else:
        print('File does not exist')


def get_stats(event):
    res = getAllAngles(event)
    total = len(res)
    return {'total': total}


def lambda_handler(event, context):
    eventname = ''
    if event['queryStringParameters'] and 'event' in event['queryStringParameters']:
        eventname = event['queryStringParameters']['event']

    if (event['resource'] == '/histogram'):
        payload = getHistogram(eventname)
    elif (event['resource'] == '/top/{x}'):
        index = int(event['pathParameters']['x'])
        if index > 3:
            body = {'message': 'Can only fetch top 3.'}
            return {'statusCode': 200, 'headers': headers, 'body': json.dumps(body)}
        payload = getTopXThatExist(index, eventname)
    elif (event['resource'] == '/thumbs/{id}'):
        payload = getByID(event['pathParameters']['id'])    
    elif (event['resource'] == '/stats'):
        body = get_stats(eventname)
        return {'statusCode': 200, 'headers': headers, 'body': json.dumps(body)}
    else:
        body = {'message': 'Task value is invalid.'}
        return {'statusCode': 400, 'headers': headers, 'body': json.dumps(body)}

    headers['Content-type'] = 'image/jpeg'

    return {
      'isBase64Encoded': True,
      'statusCode': 200,
      'headers': headers,
      'body': payload
    }


def main():
    x= {'resource': '/thumbs/{id}', 'pathParameters': {'id': 15449}, 'queryStringParameters': {'event': 'test'}}
    x= {'resource': '/histogram', 'queryStringParameters': {'event': 'test'}}
    #x= {'resource': '/top/{x}', 'pathParameters': {'x': 3}, 'queryStringParameters': {'event': 'test'}}
    x= {'resource': '/stats', 'queryStringParameters': {'event': 'test'}}

    lambda_handler(x, None)

if __name__ == '__main__':
    main()
