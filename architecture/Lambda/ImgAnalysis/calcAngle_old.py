import argparse
import boto3
import json
import os
import random
import warnings
import matplotlib.pyplot as plt
import numpy as np

from base64 import b64decode, b64encode
from botocore.exceptions import ClientError
from math import atan2, pi
from scipy.signal import savgol_filter
from skimage import io
from skimage.color import label2rgb, rgb2gray
from skimage.draw import circle_perimeter,circle
from skimage.filters import threshold_otsu,sobel
from scipy.ndimage import gaussian_filter
from skimage.filters.rank import median
from skimage.transform import resize
from skimage.measure import label, regionprops
from skimage.morphology import closing, disk, binary_dilation, reconstruction, skeletonize, square, watershed
from skimage.util import invert



##########
# hardcoded parameters
##########
#bucket="hitchhikersthumb"
bucket="hitchhikersthumbprod"
prefix="UploadedImages/"
dynamo_name="HitchhikersThumb"
SERVICES=True

###############

def write_to_file(save_path, data):
  with open(save_path, "wb") as f:
    f.write(b64decode(data))

def lambda_handler(event, context):
    if(SERVICES):
        print("EVENT:",event)

    id=random.randint(1, 100001)
    tmp_img_file="/tmp/img"+str(id)+"input.jpg"

    encodedImgString=event["body"]
    e=encodedImgString
    if(encodedImgString.find("base64")>-1):
        e=encodedImgString.split("base64,")[1]
    write_to_file(tmp_img_file, e)

    #image_64_decode = base64.decodestring(base64.b64decode(event["body"]))
    #hand = open(tmp_img_file, 'wb')
    #hand.write(image_64_decode)


    hand = io.imread(tmp_img_file)
    skeleton,cropped_small_bw,smooth=processimage2(hand)
    tx1,ty1,tx2,ty2=findThumbVector(skeleton)
    bx1,by1,bx2,by2=findBaseVector(skeleton)
    angle=calculateAngle(tx1,ty1,tx2,ty2,bx1,by1,bx2,by2)
    print("angle is", angle)

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(dynamo_name)
    #id=uuid.uuid4().int

    plotImage(cropped_small_bw,smooth,skeleton,ty1,ty2,tx1,tx2,bx1,by1,bx2,by2,"/tmp/","img"+str(id)+"result")

    if(SERVICES):
        table.put_item(
            Item={
                'id': id,
                'angle': int(angle),
                'event': "test"
    #            'img_vectors':"https://s3-ap-southeast-2.amazonaws.com/"+bucket+"/"+prefix+"img"+str(id)+".jpeg"
            }
        )
        print("PutItem succeeded:")


    return {
        'statusCode': 200,
         'headers': {
             "Access-Control-Allow-Origin": "*"
        #"Access-Control-Allow-Headers": "Content-Type",
        #     'Content-Type': 'application/json',
        #     'Access-Control-Allow-Origin': '*',
        #     'Access-Control-Allow-Credentials': 'true'
         },
        'body': json.dumps({"angle":angle,"id":id})
    }

import warnings
warnings.filterwarnings("ignore")



################################################################################
#
# Function getting image from DynamoDB and processing it to add angle image
# and calculate angle
#
################################################################################

def crop(hand,bboxTop,bboxLeft,bboxHeight,bboxWidth):
    imageHeight=hand.shape[0]
    imageWidth=hand.shape[1]
    x1=int(imageHeight*bboxTop)
    y1=int(imageWidth*bboxLeft)
    x2=int(x1+imageHeight*bboxHeight)
    y2=int(y1+imageWidth*bboxWidth)

    print(x1,y1, x2,y2)
    cropped = hand[x1:x2,y1:y2]
    return cropped


def processimage2(hand):

    #crop goes here
    mywidth=200
    cropped_small=hand
    if hand.shape[0]!=mywidth:
        wpercent = (mywidth/float(hand.shape[0]))
        hsize = int((float(hand.shape[1])*float(wpercent)))
        cropped_small = resize(hand, (mywidth,hsize), anti_aliasing=False)

    # make it a gray array out of ints
    cropped_small_bw = rgb2gray(cropped_small)

    #thresholding with watershed
    elevation_map = sobel(cropped_small_bw)
    #backgroud seeds
    circleratio=min(cropped_small_bw.shape)*0.1
    markers = np.zeros_like(cropped_small_bw)
    rr, cc = circle(0, 0, circleratio, cropped_small_bw.shape)
    markers[rr, cc] = 1 # background
    rr, cc = circle(0, cropped_small_bw.shape[1]-1, circleratio,cropped_small_bw.shape)
    markers[rr, cc] = 1 # background
    #foreground seeds
    seedx=cropped_small_bw.shape[0]*0.7
    seedy=cropped_small_bw.shape[1]*0.5
    rr, cc = circle(seedx, seedy, circleratio,cropped_small_bw.shape)
    markers[rr, cc] = 2


    segmentation = watershed(gaussian_filter(elevation_map,1), markers)

    # fill holes
    seed = np.copy(segmentation)
    seed[1:-1, 1:-1] = segmentation.max()
    mask = segmentation
    filled = reconstruction(seed, mask, method='erosion')
    filled[filled == 1] = -1
    filled[filled == 2] = 1

    #smooth
    smooth=median(filled, disk(5))
    smooth[smooth == 255] = 1

    if (smooth.min()==smooth.max()):
        sys.exit("thresholding failed")

    # skeletonize
    skeleton = skeletonize(smooth)

    return (skeleton,cropped_small_bw,smooth)


#############
# gets original image and processes it (smooth ect.) and generate
# skeleton
#############
def processimage(hand):

    cropped=hand

    #crop goes here
    mywidth=200
    cropped_small=cropped
    if cropped.shape[0]!=mywidth:
        wpercent = (mywidth/float(cropped.shape[0]))
        hsize = int((float(cropped.shape[1])*float(wpercent)))
        cropped_small = resize(cropped, (mywidth,hsize), anti_aliasing=False)

    # make it a gray array out of ints
    cropped_small_bw = rgb2gray(cropped_small)

    #threshold image
    thresh = threshold_otsu(cropped_small_bw)
    bw_thresh = closing(cropped_small_bw > thresh, square(3))

    #pick the largest area
    label_image = label(bw_thresh)
    image_label_overlay = label2rgb(label_image, image=bw_thresh)
    area=[]
    for region in regionprops(label_image):
        area.append((region.area,region.label))
    m=max(area,key=lambda item:item[0])
    largest = closing(label_image == m[1])

    # fill holes
    seed = np.copy(largest)
    seed[1:-1, 1:-1] = largest.max()
    mask = largest
    filled = reconstruction(seed, mask, method='erosion')

    smooth=median(filled, disk(5))
    smooth[smooth == 255] = 1

    # #smooth images
    # smooth = (morphology.binary_dilation(thresh, selem=square(10), out=None)).astype(np.uint8)
    # if smooth.min()==smooth.max():
    #     print("too agressive smoothing!")
    #     smooth = (morphology.binary_dilation(thresh, selem=square(1), out=None)).astype(np.uint8)

    # skeletonize
    skeleton = skeletonize(smooth)

    return (skeleton,cropped_small_bw,smooth)

###########33
# Gets the skeletonized image and clculates the angles from it
# returns xy values of the two points defining the thumb vector
############33
def findThumbVector(skeleton):
    ary,arx=np.where(skeleton == True)

    #sort by y value to get the left most point
    sortorder = ary.argsort()
    arx = arx[sortorder]
    ary = ary[sortorder]

    ih=skeleton.shape[0]
    il=skeleton.shape[1]
    # find the smallest x value in the top left bit of the image
    s=0
    while ((arx[s]>ih*0.4 or arx[s]<ih*0.05) or (ary[s]>il*0.7 or ary[s]<il*0.05)):
        s+=1
        if (s==len(arx)):
            print("finding smallest element failed")
            return(0,0,0,0)

    px=arx[s]
    py=ary[s]

    #traverse the path to get the slope from point to point
    aslope,arxc,aryc=[],[],[]
    s=s+1
    while s < len(arx):
        x=arx[s]
        y=ary[s]
        # drop points that are too far away to connect
        if x-px>ih*0.1 or y-py>il*0.1:
            s=s+1
            continue
        dx=x-px
        dy=y-py
        dx=max(x,px)-min(x,px)
        dy=max(y,py)-min(y,py)
        sl = dy/dx
        # if line is vertical set to 100
        if sl == float("inf"):
            sl=100
        aslope.append(sl)
        arxc.append(x)
        aryc.append(y)
        px=x
        py=y
        s=s+1

    #slope_smooth=aslope # don't smooth
    slope_smooth = savgol_filter(aslope, 5, 1)
    #for s in range(0,len(slope_smooth)):
    #    print("({} {}) {} {}".format(arxc[s],aryc[s],aslope[s],slope_smooth[s]))

    ps=slope_smooth[0]
    s=1
    while s < len(slope_smooth):
        #print("{} ps {}, {}>{} at ({},{})".format(s, ps,slope_smooth[s],((ps)+abs(ps)*0.10),arxc[s],aryc[s]))
        if (slope_smooth[s]>ps+abs(ps)*0.10):
            break;
        ps=slope_smooth[s]
        s+=1

    # tx1,ty1,tx2,ty2
    return (arxc[0],aryc[0],arxc[s],aryc[s])

def findBaseVector(skeleton):
    #select only the top bit of the image
    stop=int(len(skeleton)/3)
    start=int(len(skeleton)/100)
    select=skeleton[start:stop ,  :]
    m=max(select.sum(0))
    baseline = np.where(select.sum(0) == max(select.sum(0)))[0][0]
    #print("base line {}".format(baseline))

    # if this does not yield anything go back to original image
    if baseline==0:
        m=max(skeleton.sum(0))
        baseline = np.where(skeleton.sum(0) == max(skeleton.sum(0)))[0][0]
    #    print("base line {}".format(baseline))

    return(baseline,0,baseline,skeleton.shape[0]-1)

def plotImage(cropped_small_bw,smooth,skeleton,ty1,ty2,tx1,tx2,bx1,by1,bx2,by2,localdir,file_name):

    #get intersection of thumb vector with thumb baseline
    dx=(tx2-tx1)+0.00001
    dy=(ty2-ty1)+0.00001
    s=dy/dx
    b=ty1-s*tx1
    iy=s*bx1+b

    plt.figure(figsize=(4,6))

    label_image = label(smooth)
    img = label2rgb(label_image, image=cropped_small_bw)
    rr, cc = circle_perimeter(ty1, tx1, int(min(smooth.shape)*0.05),'bresenham',img.shape)
    img[rr, cc] = 1
    rr, cc = circle_perimeter(int(iy), bx1, int(min(smooth.shape)*0.05),'bresenham',img.shape)
    img[rr, cc] = 1

    # plot seed for watershed
    circleratio=int(min(smooth.shape)*0.1)
    seedx=int(smooth.shape[0]*0.7)
    seedy=int(smooth.shape[1]*0.5)
    rr, cc = circle_perimeter(seedx, seedy, circleratio,'bresenham',img.shape)
    img[rr, cc, 1] = 1

    plt.plot((tx1,tx2),(ty1,ty2), '-r')
    plt.plot((bx1,bx2),(by1,by2), '-b')
    plt.plot((tx1,bx1),(ty1,iy), '-y')

    io.imshow(img)

    plt.savefig(localdir+file_name+".jpeg")
    if(SERVICES):
        print(localdir+file_name+".jpeg",bucket,prefix+file_name+".jpeg")
        uploadtoS3(localdir+file_name+".jpeg",bucket,prefix+file_name+".jpeg")
        print("uploaded")



#tx1,ty1,tx2,ty2 and bx1,by1,bx2,by2
def calculateAngle(A1x,A1y,A2x,A2y,B1x,B1y,B2x,B2y):
    print("A: ({},{}) ({},{}) B:({} {}) ({} {})".format(A1x,A1y,A2x,A2y,B1x,B1y,B2x,B2y))
    dAx = A2x - A1x;
    dAy = A2y - A1y;
    dBx = B2x - B1x;
    dBy = B2y - B1y;
    #print("{} {} {} {}".format(dAx,dAy,dBx,dBy))

    angle = atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy);
    if(angle < 0):
        angle = angle * -1;
    degree_angle = angle * (180 / pi);
    return (degree_angle)

def uploadtoS3(file_name,bucket,object_name):
    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'EntityAlreadyExists':
            print("User already exists")
        else:
            print("Unexpected error: %s" % e)

def main():

    global SERVICES
    SERVICES=False

    parser = argparse.ArgumentParser(description='Process Thumbs.')
    parser.add_argument('--input', type=str, help='input file')
    parser.add_argument('--output', type=str, help='output file')
    parser.add_argument('--lambdas', action='store_true', help='run image file')

    args = parser.parse_args()

    print("starting calculation")
    #filename = os.path.join('../../images/','IMG_7174_small.jpg') #18
    #filename = os.path.join('../../images/','IMG_7174.jpg') #23.19859051364819
    #filename = os.path.join('../../images/holdout','thubm1.jpeg') #90
    #filename = os.path.join('../../images/','IMG_7184.jpg')
    #filename = os.path.join('../../images/','IMG_7187.jpg')
    #filename = os.path.join('../../images/','IMG_20190502_120207129.jpg')
    filename=args.input
    outfilename=args.output
    print(args.lambdas)

    with open(filename, 'rb') as open_file:
        byte_content = open_file.read()

    base64_bytes = b64encode(byte_content)
    base64_string = base64_bytes.decode('utf-8')
    lambdaEvent=json.loads('{"body":"'+base64_string+'"}')

        # with open(filename, "rb") as image_file:
        #     encoded_string = base64.b64encode(image_file.read())
        #     base64_string = base64_bytes.decode(ENCODING)
        #     lambdaEvent=json.dumps({"body":base64_string})
        #print(encoded_string)

    lambda_handler(lambdaEvent, None)


    # hand = io.imread(filename)
    # skeleton,cropped_small_bw,smooth=processimage(hand)
    # tx1,ty1,tx2,ty2=findThumbVector(skeleton)
    # bx1,by1,bx2,by2=findBaseVector(skeleton)
    # angle=calculateAngle(tx1,ty1,tx2,ty2,bx1,by1,bx2,by2)
    # print("angle is", angle)
    #
    # plotImage(cropped_small_bw,smooth,skeleton,ty1,ty2,tx1,tx2,bx1,by1,bx2,by2,outfilename,"img",False)

if __name__ == "__main__":
    main()
