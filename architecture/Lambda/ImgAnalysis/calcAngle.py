import argparse
import boto3
import json
import os
import random
import string
import warnings
import matplotlib.pyplot as plt
import numpy as np

from base64 import b64decode, b64encode
from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError
from io import BytesIO
from math import atan2, pi
from scipy.signal import savgol_filter
from skimage.io import imread, imshow
from skimage.color import label2rgb, rgb2gray
from skimage.draw import circle_perimeter,circle
from skimage.filters import threshold_otsu,sobel
from scipy.ndimage import gaussian_filter
from skimage.filters.rank import median
from skimage.transform import resize
from skimage.measure import label, regionprops
from skimage.morphology import closing, disk, binary_dilation, reconstruction, skeletonize, square, watershed
from skimage.util import invert

##########
# hardcoded parameters
##########
prefix = 'UploadedImages/'
default_bucket = 'hitchhikersthumbprod'
default_table = 'HitchhikersThumb'
headers = {'Access-Control-Allow-Origin': '*'}
services = True if os.environ.get('AWS_EXECUTION_ENV') else False
###############

warnings.filterwarnings("ignore")

thumbs_table = os.getenv('THUMBS_TABLE', default_table)
thumbs_bucket = os.getenv('THUMBS_BUCKET', default_bucket)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(thumbs_table)
s3 = boto3.client('s3')


################################################################################
#
# Function getting image from DynamoDB and processing it to add angle image
# and calculate angle
#
################################################################################

def crop(hand,bboxTop,bboxLeft,bboxHeight,bboxWidth):
    imageHeight=hand.shape[0]
    imageWidth=hand.shape[1]
    x1=int(imageHeight*bboxTop)
    y1=int(imageWidth*bboxLeft)
    x2=int(x1+imageHeight*bboxHeight)
    y2=int(y1+imageWidth*bboxWidth)

    print(x1,y1, x2,y2)
    cropped = hand[x1:x2,y1:y2]
    return cropped


#############
# gets original image and processes it (smooth ect.) and generate
# skeleton
#############
def processimage(hand):

    #crop goes here
    mywidth = 200
    wpercent = float(mywidth) / float(hand.shape[0])
    hsize = int(float(hand.shape[1]) * wpercent)
    cropped_small = resize(hand, (mywidth, hsize), anti_aliasing=False)

    # make it a gray array out of ints
    cropped_small_bw = rgb2gray(cropped_small)

    #thresholding with watershed
    elevation_map = sobel(cropped_small_bw)
    #backgroud seeds
    circleratio=min(cropped_small_bw.shape)*0.1
    markers = np.zeros_like(cropped_small_bw)
    rr, cc = circle(0, 0, circleratio, cropped_small_bw.shape)
    markers[rr, cc] = 1 # background
    rr, cc = circle(0, cropped_small_bw.shape[1]-1, circleratio,cropped_small_bw.shape)
    markers[rr, cc] = 1 # background
    #foreground seeds
    seedx=cropped_small_bw.shape[0]*0.7
    seedy=cropped_small_bw.shape[1]*0.5
    rr, cc = circle(seedx, seedy, circleratio,cropped_small_bw.shape)
    markers[rr, cc] = 2
    segmentation = watershed(gaussian_filter(elevation_map,1), markers)

    # fill holes
    seed = np.copy(segmentation)
    seed[1:-1, 1:-1] = segmentation.max()
    mask = segmentation
    filled = reconstruction(seed, mask, method='erosion')
    filled[filled == 1] = -1
    filled[filled == 2] = 1

    #smooth
    smooth = median(filled, disk(5))
    smooth[smooth == 255] = 1

    if (smooth.min() == smooth.max()):
        sys.exit("thresholding failed")

    # skeletonize
    skeleton = skeletonize(smooth)

    return (skeleton, cropped_small_bw, smooth)


###########33
# Gets the skeletonized image and clculates the angles from it
# returns xy values of the two points defining the thumb vector
############33
def findThumbVector(skeleton):
    ary,arx=np.where(skeleton == True)

    #sort by y value to get the left most point
    sortorder = ary.argsort()
    arx = arx[sortorder]
    ary = ary[sortorder]

    ih=skeleton.shape[0]
    il=skeleton.shape[1]
    # find the smallest x value in the top left bit of the image
    s=0
    while ((arx[s]>ih*0.4 or arx[s]<ih*0.05) or (ary[s]>il*0.7 or ary[s]<il*0.05)):
        s+=1
        if (s==len(arx)):
            print('finding smallest element failed')
            return(0, 0, 0, 0)

    px=arx[s]
    py=ary[s]

    #traverse the path to get the slope from point to point
    aslope, arxc, aryc = [], [], []
    s = s + 1
    while s < len(arx):
        x = arx[s]
        y = ary[s]
        # drop points that are too far away to connect
        if x - px > ih * 0.1 or y - py > il * 0.1:
            s = s + 1
            continue
        dx = x - px
        dy = y - py
        dx = max(x, px) - min(x, px)
        dy = max(y, py) - min(y, py)
        sl = dy/dx
        # if line is vertical set to 100
        if sl == float('inf'):
            sl = 100
        aslope.append(sl)
        arxc.append(x)
        aryc.append(y)
        px = x
        py = y
        s = s + 1

    slope_smooth = savgol_filter(aslope, 5, 1)
    #for s in range(0,len(slope_smooth)):
    #    print("({} {}) {} {}".format(arxc[s],aryc[s],aslope[s],slope_smooth[s]))

    ps=slope_smooth[0]
    s=1
    while s < len(slope_smooth):
        #print("{} ps {}, {}>{} at ({},{})".format(s, ps,slope_smooth[s],((ps)+abs(ps)*0.10),arxc[s],aryc[s]))
        if (slope_smooth[s]>ps+abs(ps)*0.10):
            break;
        ps=slope_smooth[s]
        s+=1

    # tx1,ty1,tx2,ty2
    return (arxc[0],aryc[0],arxc[s],aryc[s])


def findBaseVector(skeleton):
    #select only the top bit of the image
    stop=int(len(skeleton)/3)
    start=int(len(skeleton)/100)
    select=skeleton[start:stop ,  :]
    m=max(select.sum(0))
    baseline = np.where(select.sum(0) == max(select.sum(0)))[0][0]
    #print("base line {}".format(baseline))

    # if this does not yield anything go back to original image
    if baseline==0:
        m=max(skeleton.sum(0))
        baseline = np.where(skeleton.sum(0) == max(skeleton.sum(0)))[0][0]
    #    print("base line {}".format(baseline))

    return(baseline,0,baseline,skeleton.shape[0]-1)


def plotImage(cropped_small_bw,smooth,skeleton,ty1,ty2,tx1,tx2,bx1,by1,bx2,by2):
    #get intersection of thumb vector with thumb baseline
    dx=(tx2-tx1)+0.00001
    dy=(ty2-ty1)+0.00001
    s=dy/dx
    b=ty1-s*tx1
    iy=s*bx1+b

    plt.clf()
    fig_size = plt.rcParams["figure.figsize"]
    fig_size[1] = 8
    # plot mask and angles
    label_image = label(smooth)
    img = label2rgb(label_image, image=cropped_small_bw)
    rr, cc = circle_perimeter(ty1, tx1, int(min(smooth.shape)*0.05),'bresenham',img.shape)
    img[rr, cc] = 1
    rr, cc = circle_perimeter(int(iy), bx1, int(min(smooth.shape)*0.05),'bresenham',img.shape)
    img[rr, cc] = 1

    # plot seed for watershed
    circleratio=int(min(smooth.shape)*0.1)
    seedx=int(smooth.shape[0]*0.7)
    seedy=int(smooth.shape[1]*0.5)
    rr, cc = circle_perimeter(seedx, seedy, circleratio,'bresenham',img.shape)
    img[rr, cc, 1] = 1

    plt.plot((tx1,tx2), (ty1,ty2), '-r')
    plt.plot((bx1,bx2), (by1,by2), '-b')
    plt.plot((tx1,bx1), (ty1,iy), '-y')

    imshow(img)

    f = BytesIO()
    plt.savefig(f, format='jpg', bbox_inches = 'tight', pad_inches = 0)
    f.seek(0)
    return f


#tx1,ty1,tx2,ty2 and bx1,by1,bx2,by2
def calculateAngle(A1x,A1y,A2x,A2y,B1x,B1y,B2x,B2y):
    print("A: ({},{}) ({},{}) B:({} {}) ({} {})".format(A1x,A1y,A2x,A2y,B1x,B1y,B2x,B2y))
    dAx = A2x - A1x
    dAy = A2y - A1y
    dBx = B2x - B1x
    dBy = B2y - B1y
    #print("{} {} {} {}".format(dAx,dAy,dBx,dBy))

    angle = atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy)
    if(angle < 0):
        angle = angle * -1
    degree_angle = angle * (180 / pi)
    return (degree_angle)


def uploadtoS3(file, bucket, object_name):
    try:
        response = s3.upload_fileobj(file, bucket, object_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'EntityAlreadyExists':
            print("Already exists")
        else:
            print("Unexpected error: %s" % e)


def create_presigned_url(bucket, object_name, expiration=3600):
    try:
        response = s3.generate_presigned_url(
            'get_object',
            Params={'Bucket': bucket, 'Key': object_name},
            ExpiresIn=expiration
        )
    except ClientError as e:
        logging.error(e)
        return None
    return response


def calculate_percentile(angle, event):
    a = table.scan(FilterExpression=Attr("event").eq(event))
    lessthan_angles = len([i['angle'] for i in a['Items'] if i['angle'] < angle])
    all_angles = len(a['Items'])
    print(lessthan_angles, '/', all_angles)
    return  lessthan_angles / all_angles * 100


def gen_id(length = 16):
    #''.join(random.choices(string.ascii_letters + string.digits, k=length))
    return int(''.join(random.choices(string.digits, k=length)))


def lambda_handler(event, context):
    id = gen_id()

    s3_file_path = '{}/img{}.jpg'.format(thumbs_bucket, id)

    if not (event['queryStringParameters'] and 'event' in event['queryStringParameters']):
        body = {'message': 'event parameter is required'}
        return {'statusCode': 400, 'headers': headers, 'body': json.dumps(body)}

    eventname = event['queryStringParameters']['event']

    e = event['body'].split('base64,')[1] if event['body'].find('base64') > -1 else event['body']

    f = BytesIO(b64decode(e))

    hand = imread(f, plugin='pil')

    try:
        skeleton, cropped_small_bw,smooth = processimage(hand)
        tx1, ty1, tx2, ty2 = findThumbVector(skeleton)
        bx1, by1, bx2, by2 = findBaseVector(skeleton)
        angle = calculateAngle(tx1, ty1, tx2, ty2, bx1, by1, bx2, by2)
    except:
        body = {'message': 'Failed to calculate angle. Please try again with a new photo.'}
        return {'statusCode': 400, 'headers': headers, 'body': json.dumps(body)}

    print("angle is", angle)

    f_new = plotImage(cropped_small_bw,smooth,skeleton,ty1,ty2,tx1,tx2,bx1,by1,bx2,by2)

    percentile, image_url = 0, ''

    if(services):
        uploadtoS3(f_new, thumbs_bucket, s3_file_path)
        image_url = create_presigned_url(thumbs_bucket, s3_file_path)
        percentile = calculate_percentile(angle, eventname)
        table.put_item(
            Item={
                'id': id,
                'angle': int(angle),
                'event': eventname
            }
        )

    body = {'angle':angle, 'percentile': percentile, 'id': id, "image": image_url}
    return {'statusCode': 200, 'headers': headers, 'body': json.dumps(body)}


def main():
    parser = argparse.ArgumentParser(description='Process Thumbs.')
    parser.add_argument('--input', type=str, help='input file')
    parser.add_argument('--output', type=str, help='output file')
    parser.add_argument('--lambdas', action='store_true', help='run image file')

    args = parser.parse_args()

    print("starting calculation")
    #filename = os.path.join('../../images/','IMG_7174_small.jpg') #18
    #filename = os.path.join('../../images/','IMG_7174.jpg') #23.19859051364819
    #filename = os.path.join('../../images/holdout','thubm1.jpeg') #90
    #filename = os.path.join('../../images/','IMG_7184.jpg')
    #filename = os.path.join('../../images/','IMG_7187.jpg')
    #filename = os.path.join('../../images/','IMG_20190502_120207129.jpg')
    filename=args.input
    outfilename=args.output
    print(args.lambdas)

    with open(filename, 'rb') as open_file:
        byte_content = open_file.read()

    base64_bytes = b64encode(byte_content)
    base64_string = base64_bytes.decode('utf-8')
    lambdaEvent = {'queryStringParameters': {'event': 'test'}, 'body': base64_string}

    lambda_handler(lambdaEvent, None)

if __name__ == "__main__":
    main()
