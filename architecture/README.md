##########
# Lambda Layers
################
yum install python3
python3 -m venv np-env
source np-env/bin/activate
pip install numpy
pip install scikit-image
pip install matplotlib
pip install boto3
pip freeze > requirements.txt

pip install -r requirements.txt --no-deps -t tmpDir
zip -r thumbs_layer.zip tmpDir

# did not work
aws lambda publish-layer-version --layer-name tropo_test --zip-file fileb://tropo_layer.zip

# API for GET task=
https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-lambda-non-proxy-integration.html

#Setting up an image app with API for Post
https://itnext.io/create-a-highly-scalable-image-processing-service-on-aws-lambda-and-api-gateway-in-10-minutes-7cbb2893a479

#API logging
https://kennbrodhagen.net/2016/07/23/how-to-enable-logging-for-api-gateway/

#############
# Trigger API
############
curl -X POST --data-binary @hitchhikersthumb/images/IMG_7174_small.jpg https://n3pz86qu50.execute-api.ap-southeast-2.amazonaws.com/HThumbDeploy
{"angle": 18.43494882292201, "id": 73383}
