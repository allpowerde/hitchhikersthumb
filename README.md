
# Idea

We want to create a web-app that lets people automatically calculate how much of a Hitchhiker's Thumb they have. This can be used during presentations to make genomics more personal.

Denis' typical presentations contain a segment where everyone holds out their thumb. Usually there are about 1 in 3 that have a very bent thumb and everybody loves to spot them. So, rather than seeing only people around them, the app lets the audience submit a picture of their thumb during the presentation and the big screen on stage would then display the most impressive thumbs in the room in realtime.

![Presentation idea](/images/docuimage/idea.png)

The thunb's bendiness can be calculated by first identifying the thumb in an image and then segmentation it to fit a skeleton and calculate the angle in the shortest segment.

 ![Segmentation](/images/docuimage/segmentation.png)

# Approach

We need to build three components

1. Thumb detection model
2. Image segmentation and angle calculator
3. Webpage to upload images and trigger analysis
4. Presentation webpage to visualize results on stage

# C1. Training Dataset for thumb recognision

*STATUS*: Postponed.


AWS rekognision will have to be retrained as currently it can only detect "fingers" rather than identify "thumbs" specifically.

 ![Segmentation](/images/docuimage/Rekognition_console.png)

## Dataset

*STATUS*: Denis has access to dataset, however this element will be postponed because the training will cost a lot of money due to the file sizes.

Google has a dataset containing >400,000 images from 31 participants and 30 different environment. The dataset contains 8 gesture classes, which are:

* left_hand, right_hand: almost closed fist.
* left_peace, right_peace: peace sign.
* left_thumbs_up, right_thumbs_up: thumbs up.
* left_thumbs_down, right_thumbs_down: thumbs down.

Each image is annotated with gesture name and bounding box (coordinates normalized between 0 and 1).

Annotations for images in the same folder are saved in labels.csv with the following format: image_name, gesture_label, bb_x, bb_y, bb_w, bb_h.

https://sites.google.com/view/hmd-gesture-dataset


## Training using sagemaker

*STATUS*: Postponed

This [notebook](notebook/object_detection_image_json_format.ipynb) contains current attempts at retraining using a toy dataset until the real dataset is available.

# C2. Segmentation

*STATUS*: DONE

* This [notebook](notebook/segmetThumb.ipynb) contains current attempts at segmenting.
* This is the actual [Lambda Function](architecture/Lambda/ImgAnalysis/calcAngle.py).
* Results look like this.
 ![Segmentation](architecture/webpage/img/example.jpg)

# C3. Webpage

*STATUS*: webpage designed, deployed and talking to API yet, however only the angle rather than the returned image is displayed

This is the webpage [http://tinyurl.com/CSIROThumb](webpage).

![WebpageSubmit](images/docuimage/webpageSubmit.jpg)
![WebpageResult](images/docuimage/webpageResult.jpg)


API-Gateway can be triggered like so

### 1. Submit image

POST API call to submit images:

```curl -X POST --data-binary @hitchhikersthumb/images/IMG_7174_small.jpg https://ee4ya3gjrg.execute-api.ap-southeast-2.amazonaws.com/HThumbDeployImg```

response

 ```{"angle": 18.43494882292201, "id": 73383}```

### 2. POST API call to get processed image back

POST API call to collect image and results (via postman)

```
https://ee4ya3gjrg.execute-api.ap-southeast-2.amazonaws.com/HThumbDeploy/ret?task=getById&value=4102
```

 ![Segmentation](/images/docuimage/cloudcraft.png)


# C4 Presentation page
*STATUS*: API calls ready but page need to be designed

### 1. Histogram of all submitted angles

POST API call to collect image and results (via postman)

```
https://ee4ya3gjrg.execute-api.ap-southeast-2.amazonaws.com/HThumbDeploy/ret?task=histogram
```

### 2. Top Images

POST API call to collect image and results (via postman)

task=top&value=X, X can be any number intended was 1,2 or 3 for the top three images to be returned individually

```
https://ee4ya3gjrg.execute-api.ap-southeast-2.amazonaws.com/HThumbDeploy/ret?task=top&value=1
```

# Alternative approaches

* We could train a model directly to segment the data, however we would have to manually label images [Medium Blog](https://medium.com/nanonets/how-to-do-image-segmentation-using-deep-learning-c673cc5862ef)
* Also a good approach and this one is in [python](https://gogul09.github.io/software/hand-gesture-recognition-p1)
